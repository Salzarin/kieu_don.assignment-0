#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

//Define Max Stack Size, technically for a 23 by 23 it should be 523, but 
//I used approximately double in case for some reason we needed to 
//keep checking. 
#define MAXSTACK 1000

struct posData{
	bool valid;
	int x;
	int y;
};

struct stack{
	int top;
	struct posData _data[MAXSTACK];	
};

typedef struct stack stk;
stk pileCheck;

void init_stack(){
	pileCheck.top = -1;
}
void push(struct posData data){
	if(pileCheck.top == (MAXSTACK -1)){
		printf("Warning Stack is Filled!");	
	}
	else{
		pileCheck.top++;
		data.valid=true;
		pileCheck._data[pileCheck.top] = data;
	}
}
struct posData pop(){
	if(pileCheck.top == -1){
		struct posData Throwaway;
		Throwaway.x = -1;
		Throwaway.y = -1;
		Throwaway.valid =false;
		return Throwaway;
	}
	else{	
		int num = pileCheck.top;
		pileCheck.top--;
		return pileCheck._data[num];	
	}	

}

int check_stack(){
	if(pileCheck.top == -1){
	return 0;
	} 
	return 1;
}
