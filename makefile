all: SandPile rand_3tuple

SandPile: SandPile.o
	gcc -Wall -Werror -o SandPile SandPile.c
rand_3tuple:
	gcc -Wall -Werror -o rand_3tuple rand_3tuple.c
clean:
	rm -f SandPile.o SandPile rand_3tuple rand_3tuple.o *~
