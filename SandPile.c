#include "stack.h"
#define MAXFIELDX 23
#define MAXFIELDY 23
int main(int argc, char *argv[]){
	
	
	int WorkField[MAXFIELDX][MAXFIELDY] = {{0}}; //Create our work field where the sand piles are at;
	int count;
	
	
	if(argc > 1){ // Double Check if we even have anything arguments otherwise, skip this.
		
		//intitally set these to fail
		int x = MAXFIELDX+1;
		int y = MAXFIELDY+1;
		int h = 5;
		
		for(count = 0; count<argc; count++){ // Interate through our arguments.
			
			
			int index = (count-1) % 3; // Modulus Divide to get whether we're looking at x, y, or h;

			switch(index){
				case 0:
					x = atoi(argv[count]);
					break;
				case 1:
					y = atoi(argv[count]);
					break;
				case 2:
					//Form Pile here as this implies we have x and y;
					h = atoi(argv[count]);
					//Error Check if x<23, y<23, and z<4
					if( x<MAXFIELDX && y<MAXFIELDY && h <= 4){
						//Check if Pile Exists before setting drop
						if(WorkField[x][y] == 0){
							WorkField[x][y] = h; //Place Drop at Specified Coordinates then reset
							printf("Pile dropped at %d, %d with height of %d \n",x,y, h);
							x = 24;
							y = 24;
							h = 5;
							
						}
						else{
							printf("Pile Exists at %d, %d\n",x,y);
						}
						
					}
					else{
						printf("Improper drop at %d, %d using height of %d\n",x,y,h);
					}
							
					
					break;		
				}

		}
		
	}
	else{
		printf("No Piles Added\n");
	}
	
	init_stack();
	while(1){
		//Print out our current Map
		int i;
		int j;
		//printf("\033[2J"); // Clear the Terminal --- Currently doesn't work well depending on speed of system.
		for(i = 0; i<MAXFIELDX;i++){
			
			for(j = 0; j<MAXFIELDY;j++){

				switch(WorkField[i][j]){
					case 0://Black
					printf("\x1b[30;40m%d\x1b[0m ",WorkField[i][j]);
					break;
					case 1://Red				
					printf("\x1b[30;41m%d\x1b[0m ",WorkField[i][j]);			
					break;
					case 2:	//Yellow			
					printf("\x1b[30;43m%d\x1b[0m ",WorkField[i][j]);			
					break;
					case 3:	//Green			
					printf("\x1b[30;42m%d\x1b[0m ",WorkField[i][j]);			
					break;
					case 4:	//Blue		
					printf("\x1b[30;44m%d\x1b[0m ",WorkField[i][j]);			
					break;
					default:
					break;
				}
			}
			printf("\n");
		}
		printf("\n");
		usleep(1000);
		
		//Find the Center of Field. Since we start with 0 being the first index, the -1 is implicit.
		int mid_x = (MAXFIELDX)/2;
		int mid_y = (MAXFIELDY)/2;
		//Drop a piece of Sand at the Center
		WorkField[mid_x][mid_y]++;
		//Put this pile position on the stack to check topple.
		struct posData temp;
		temp.x = mid_x;
		temp.y = mid_y;	
		push(temp);


		while(check_stack()){
			temp = pop();

			
			//Double check that we have a valid pile
			if(temp.valid){
				//Check if it Topples
				if(WorkField[temp.x][temp.y] > 4){
					//It toppled, add to Surroundings, and push to stack to check those piles.
				
					if((temp.y +1)<MAXFIELDY){ //Error Check to see if we're still in bounds.
				
					struct posData up;
					up.x = temp.x;
					up.y = temp.y+1;
					WorkField[temp.x][temp.y+1]++;
					push(up);
					}
				
					if((temp.y-1)>-1){
				
					struct posData down;
					down.x = temp.x;
					down.y = temp.y-1;
					WorkField[temp.x][temp.y-1]++;
					push(down);
					}

					if((temp.x+1)< MAXFIELDX){
					struct posData right;
					right.x = temp.x+1;
					right.y = temp.y;
					WorkField[temp.x+1][temp.y]++;
					push(right);
					}

					if((temp.x-1)>-1){
					struct posData left;
					left.x = temp.x-1;
					left.y = temp.y;
					WorkField[temp.x-1][temp.y]++;				
					push(left);
					}

					WorkField[temp.x][temp.y] = 1;
				}
			}
		}
	}
	
	return 0;
}
